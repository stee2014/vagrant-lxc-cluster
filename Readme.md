# vagrant-lxc-cluster

テスト用のクラスタ環境を構築する Vagrant スクリプト

以下のような仮想クラスタを構築します。

* lxc仮想Lixnux(FreeBSDのJail環境に相当)
* ubuntu64(12.04) + rcssserver-15.1.0
* master： 10.0.3.200
* s1 - 3： 10.0.3.201 - 203
* backup： 10.0.3.199

## 動作環境

以下のような環境で動かしています。

* CPU: Core2Duo 2.66GHz
* メモリ： 2Gbyte
* OS: Ubuntu64 13.10
* Vagrant 1.2.2
* vagrant-lxc プラグイン 0.7.0
* lxc 1.0.0.alpha1

## 導入準備

### [Vagrant](http://www.vagrantup.com/)のインストール
### vagrant-lxc プラグインのインストール
```
$ vagrant plugin install vagrant-lxc
```

### lxc のインストール
```
$ sudo apt-get install lxc
```

### /etc/default/lxc-net の設定。固定IPを設定するためにDHCPの範囲を変更
```
#LXC_DHCP_RANGE="10.0.3.2,10.0.3.254"
#LXC_DHCP_MAX="253"
LXC_DHCP_RANGE="10.0.3.2,10.0.3.99"
LXC_DHCP_MAX="98"
```

### lxc-net もしくは OS の再起動。
```
$ sudo restart lxc-net
```

## 使い方

### git clone でコードを取得
```
$ git clone git@bitbucket.org:stee2014/vagrant-lxc-cluster.git
$ cd vagrant-lxc-cluster
```
### VM構築および起動
```
$ vagrant up --provider lxc # 1回目：VM構築と起動
$ vagrant up # 2度目以降：構築済VMの起動
```

### 起動確認
```
$ sudo lxc-list
$ sudo lxc-ls --fancy
NAME                          STATE    IPV4                   IPV6  AUTOSTART  
----------------------------------------------------------------------------------------
lxc-ubuntu_backup-1386946852  RUNNING  10.0.3.199             -     NO
lxc-ubuntu_master-1386946852  RUNNING  10.0.3.200, 10.0.3.33  -     NO
lxc-ubuntu_s1-1386946852      RUNNING  10.0.3.80              -     NO
lxc-ubuntu_s2-1386946852      RUNNING  10.0.3.52              -     NO
lxc-ubuntu_s3-1386946852      RUNNING  10.0.3.86              -     NO
```

### ssh 接続
```
$ vagrant ssh [master|s1|s2|s3|backup]
```

### VM終了
```
$ vagrant halt
```

### VM破棄
```
$ vagrant destroy
```

### 設定済VMのbox化
```
$ vagrant package [master|s1|s2|s3|backup]
```

## 既知の問題と対策
### 最初の起動時に固定IP設定が失敗して ```vagrant up ``` が終了しない。
1. 別の端末にて ```sudo lxc-list``` で VM が RUNNING になってる事を確認する。
1. C-c C-c で```vagrant up```を強制終了する。
1. ```vagrant halt``` で VM を一旦シャットダウンする。
1. 再度```vagrant up``` で VM を起動する

## その他
### 共有フォルダ /vagrant
Vagrantfile があるフォルダは、各VMで /vagrant にマウントされ、共有フォルダとして利用できます。

### /etc/hosts
VM内の hosts には以下のようなホスト名を登録しています。
```
10.0.3.199 backup
10.0.3.200 master
10.0.3.201 s1
 :
10.0.3.209 s9
```
